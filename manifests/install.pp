/*
Copyright 2014 Universitatea de Vest din Timi�^�oara

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Adrian Spataru <spataru.florin@info.uvt.ro>
@contact: spataru.florin@info.uvt.ro
@copyright: 2014 Universitatea de Vest din Timi�^�oara
*/

# Class: scape_plato
#
# This module manages Plato instalation
#



class scape_plato::install {
	$m_root_pwd = 'scape'
	$plato_db_pwd = 'scape'
	$idb_pwd = 'scape'
	$jboss_home = '/usr/share/jboss-as'
	$mysql_connector = 'mysql-connector-java-5.1.32'
	
	$mysql_options = {
		'mysqld' => {
			'character-set-server' => 'utf8',
			'collation-server' => 'utf8_unicode_ci',
			'max_allowed_packet' => '128M',
			'max_sp_recursion_depth' => '255',
			'thread_stack' => '512K',

		}
	}
	
	class { '::mysql::server':
		root_password => $m_root_pwd,
		override_options => $mysql_options,
		restart => true,
	}
	
	package {'unzip':
		ensure => present,
	}
	
	exec {'download-plato':
		command => "wget -O /mnt/plato.zip https://github.com/openplanets/plato/archive/master.zip",
		path => '/usr/bin/',
		onlyif => "test ! -e /mnt/plato.zip",
	}
	
	exec {'unzip-plato':
		command => "unzip /mnt/plato.zip -d /mnt/",
		path => '/usr/bin/',
		onlyif => "test ! -e /mnt/plato-master",
		require => [Exec['download-plato'], Package['unzip']],
	}

	exec {'setup-database':
		command => 'bash -c "cd /mnt/plato-master/tools/;" ./setup-database.sh ${m_root_pwd} ${plato_db_pwd} ${idb_pwd}',
		path => '/bin/',
		require => Exec['unzip-plato'],
	}
	

	exec {'download-jboss':
		command => "wget -O /etc/scape/modules/jboss_as/files/jboss-as-7.1.1.Final.tar.gz http://download.jboss.org/jbossas/7.1/jboss-as-7.1.1.Final/jboss-as-7.1.1.Final.tar.gz",
		path => "/usr/bin/",
		onlyif => "test ! -e /etc/scape/modules/jboss_as/files/jboss-as-7.1.1.Final.tar.gz",
		before => Class['jboss_as'],
	}

	class {'java':}
	->
	class { 'jboss_as':
        	jboss_dist     => 'jboss-as-7.1.1.Final.tar.gz',
	        jboss_user     => 'jboss-as',
        	jboss_group    => 'jboss-as',
	        jboss_home     => $jboss_home,
        	staging_dir    => '/tmp/puppet-staging/jboss_as',
	        standalone_tpl => 'jboss_as/standalone.xml.erb'
    	}	

	exec {'copy-standalone':
		command => "bash -c \"cp /mnt/plato-master/tools/standalone.xml ${jboss_home}/standalone/configuration/standalone.xml\"",
		path => '/bin/',
		require => Class['jboss_as'],
	}

	

	file {"/etc/scape/modules/puppet-plato/files/":
		ensure => 'directory',
		recurse => true,
	}

	exec {'download-mysql-connectorJ':
		command => "wget -O /etc/scape/modules/puppet-plato/files/${mysql_connector}.tar.gz http://dev.mysql.com/get/Downloads/Connector-J/${mysql_connector}.tar.gz",
		path => '/usr/bin/',
		onlyif => "test ! -e /etc/scape/modules/puppet-plato/files/${mysql_connector}.tar.gz",
		require => [File["/etc/scape/modules/puppet-plato/files/"],Class['jboss_as']],
	}

	exec {'unzip-mysql-connectorJ':
		command => "bash -c \"cd /etc/scape/modules/puppet-plato/files/; tar -xzf ${mysql_connector}.tar.gz\"",
		path => '/bin/',
		require => Exec['download-mysql-connectorJ'],
	}

	exec {'check-jboss-presence':
	    command     => '/bin/true',
	    onlyif      => "test -e ${jboss_home}/modules/com/mysql",
	    path 	=> '/usr/bin/',
	}

	file {"${jboss_home}/modules/com/mysql":
		ensure => 'directory',
		require => Exec['check-jboss-presence'],
		recurse => true,
	}

	file {"${jboss_home}/modules/com/mysql/main":
		ensure => 'directory',
		require => File["${jboss_home}/modules/com/mysql"],
		recurse => true,
	}


	file {"${jboss_home}/modules/com/mysql/main/module.xml":
		ensure => 'present',
		source => '/etc/scape/modules/puppet-plato/templates/mysql-connector-template.xml',
		require => File["${jboss_home}/modules/com/mysql/main"],
		recurse => true,
	}

	exec {'copy-mysql-connector':
		command => "cp /etc/scape/modules/puppet-plato/files/${mysql_connector}/${mysql_connector}-bin.jar ${jboss_home}/modules/com/mysql/main/",
		path => '/bin/',
		require => [Exec['unzip-mysql-connectorJ'], File["${jboss_home}/modules/com/mysql/main"]],
	}
	

	file {"${jboss_home}/modules/org/picketlink/main":
		ensure => 'directory',
		require => Exec['check-jboss-presence'],
		recurse => true,
	}


	exec {'delete-picketlink':
		command => "rm ${jboss_home}/modules/org/picketlink/main/*.jar",
		path => '/bin/',
		require => File["${jboss_home}/modules/org/picketlink/main"],

	}

	exec {'download-picketlink-core':
		command => "wget -O ${jboss_home}/modules/org/picketlink/main/picketlink-core-2.1.4.Final.jar https://repository.jboss.org/nexus/content/groups/public/org/picketlink/picketlink-core/2.1.4.Final/picketlink-core-2.1.4.Final.jar",
		path => '/usr/bin/',
		require => Exec['delete-picketlink'],
	}

	exec {'download-picketlink-jbas':
                command => "wget -O ${jboss_home}/modules/org/picketlink/main/picketlink-jbas7-2.1.4.Final.jar https://repository.jboss.org/nexus/content/groups/public/org/picketlink/distribution/picketlink-jbas7/2.1.4.Final/picketlink-jbas7-2.1.4.Final.jar",
                path => '/usr/bin/',
                require => Exec['delete-picketlink'],
        }

	exec {'copy-picketlink-xml':
		command => "cat /etc/scape/modules/puppet-plato/templates/picketlink-template.xml > ${jboss_home}/modules/com/mysql/main/module.xml",
		path => '/bin/',
		require => Exec['download-picketlink-jbas'],
	}

	package {
	    'maven2':
	        ensure      => installed,
	        provider    => apt,
	}

	exec {'package-jboss-util':
		command => "bash -c \"cd /mnt/plato-master/jboss-util ; mvn package\"",
		path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
		require => Exec['unzip-plato'],
	}	

	file {["${jboss_home}/modules/eu", 
			"${jboss_home}/modules/eu/scape_project",
			"${jboss_home}/modules/eu/scape_project/planning", 
			"${jboss_home}/modules/eu/scape_project/planning/util",
			"${jboss_home}/modules/eu/scape_project/planning/util/main"]:
		ensure => directory,
		require => Exec['check-jboss-presence'],
		recurse => true,
	}
	
	exec {'copy-jboss-util':
		command => "cp /mnt/plato-master/jboss-util/target/*.jar ${jboss_home}/modules/eu/scape_project/planning/util/main/",
                path   => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
                require => Exec['package-jboss-util'],
	}

	file {"${jboss_home}/modules/eu/scape_project/planning/util/main/module.xml":
		ensure => 'present',
		source => "/etc/scape/modules/puppet-plato/templates/jboss_util.xml",
		require => Exec['check-jboss-presence'],
	}
	
	
}

include 'scape_plato::install'
